/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter Test Plugin (2009 - Steven Rodriguez -)         //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file testplugin.c
/// \brief Cutter test plugin.
/// \details This is the Cutter test plugin for general purpose use.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "testplugin.h"

//===========================================================//
//Global variables                                           //
//===========================================================//

struct SplitterPlugin plugin;

//===========================================================//
//Main Implementation Function                               //
//===========================================================//

struct SplitterPlugin *GetPlugin()
{
    struct Version version;

    version.Major = 0;
    version.Minor = 1;

    plugin.LoadPlugin = TestPlugin_LoadPlugin;
    plugin.ClosePlugin = TestPlugin_LoadPlugin;
    plugin.CutFile = TestPlugin_CutFile;
    plugin.PasteFile = TestPlugin_PasteFile;
    plugin.CanCut = TestPlugin_CanCut;
    plugin.CanPaste = TestPlugin_CanPaste;
    plugin.ShowAdditionalCutOptions = TestPlugin_ShowAdditionalCutOptions;
    plugin.ShowAdditionalPasteOptions = TestPlugin_ShowAdditionalPasteOptions;
    plugin.HasInterfaceSupport = TestPlugin_HasInterfaceSupport;
    plugin.Name = "Test Plugin";
    plugin.Provider = "Digitecnology";
    plugin.PluginVersion = version;
    plugin.HasAdditionalCutOptions = CUTTER_TRUE;
    plugin.HasAdditionalPasteOptions = CUTTER_TRUE;

    return &plugin;
}

//===========================================================//
//Functions                                                  //
//===========================================================//

void TestPlugin_LoadPlugin()
{
    printf("(TestPlugin) Loading plugin...\n");
}

void TestPlugin_ClosePlugin()
{
    printf("(TestPlugin) Closing plugin...\n");
}

struct SplitterOperation TestPlugin_CutFile(struct PluginSystem *system, char *fileName, Cutter_UInt64 size, char *path, Cutter_UInt8 useProgressBar)
{
    struct SplitterOperation result;

    result.Status = Finished;
    result.Message = "Done!";

    printf("(TestPlugin) Cutting... [file = %s] [path = %s] [size = %lu] [useprogressbar = %hhu]\n", fileName, path, size, useProgressBar);

    return result;
}

struct SplitterOperation TestPlugin_PasteFile(struct PluginSystem *system, char *fileName, char *path, Cutter_UInt8 useProgressBar)
{
    struct SplitterOperation result;

    result.Status = Finished;
    result.Message = "Done!";

    printf("(TestPlugin) Pasting... [file = %s] [path = %s] [useprogressbar = %hhu]\n", fileName, path, useProgressBar);

    return result;
}

Cutter_UInt8 TestPlugin_CanCut(char *fileName)
{
    return CUTTER_TRUE;
}

Cutter_UInt8 TestPlugin_CanPaste(char *fileName)
{
    return CUTTER_TRUE;
}

void *TestPlugin_ShowAdditionalCutOptions(char *interface)
{
    if(strcmp(interface,"Console") == 0)
    {
        printf("(TestPlugin) Showing additional cut options...\n");
    }

    return NULL;
}

void *TestPlugin_ShowAdditionalPasteOptions(char *interface)
{
    if(strcmp(interface,"Console") == 0)
    {
        printf("(TestPlugin) Showing additional paste options...\n");
    }

    return NULL;
}

Cutter_UInt8 TestPlugin_HasInterfaceSupport(char *interface)
{
    if(strcmp(interface,"Console") == 0)
    {
            return CUTTER_TRUE;
    }

    return CUTTER_FALSE;
}
