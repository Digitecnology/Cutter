/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter Test Plugin (2009 - Steven Rodriguez -)         //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file testplugin.h
/// \brief Cutter test plugin definitions.
/// \details This is the Cutter test plugin definitions.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "cutter.h"

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void TestPlugin_LoadPlugin()
/// \brief Loads the plugin.
////////////////////////////////////////////////////
void TestPlugin_LoadPlugin();

////////////////////////////////////////////////////
/// \fn void TestPlugin_ClosePlugin()
/// \brief Closes the plugin.
////////////////////////////////////////////////////
void TestPlugin_ClosePlugin();

////////////////////////////////////////////////////
/// \fn struct SplitterOperation TestPlugin_CutFile(struct PluginSystem *system, char *fileName, Cutter_UInt64 size, char *path, Cutter_UInt8 useProgressBar)
/// \brief Cuts the file selected.
/// \param system The plugin system to use.
/// \param fileName The file to cut.
/// \param size The size in bytes to cut.
/// \param path The path to put the parts.
/// \param useProgressBar CUTTER_TRUE if updates the progress bar, CUTTER_FALSE if don't updates it.
/// \return The splitter operation result.
////////////////////////////////////////////////////
struct SplitterOperation TestPlugin_CutFile(struct PluginSystem *system, char *fileName, Cutter_UInt64 size, char *path, Cutter_UInt8 useProgressBar);

////////////////////////////////////////////////////
/// \fn struct SplitterOperation TestPlugin_PasteFile(struct PluginSystem *system, char *fileName, char *path, Cutter_UInt8 useProgressBar)
/// \brief Pastes the file selected.
/// \param system The plugin system to use.
/// \param fileName The file to paste.
/// \param path The path to put the new file pasted.
/// \param useProgressBar CUTTER_TRUE if updates the progress bar, CUTTER_FALSE if don't updates it.
/// \return The splitter operation result.
////////////////////////////////////////////////////
struct SplitterOperation TestPlugin_PasteFile(struct PluginSystem *system, char *fileName, char *path, Cutter_UInt8 useProgressBar);

////////////////////////////////////////////////////
/// \fn Cutter_UInt8 TestPlugin_CanCut(char *fileName)
/// \brief Verifies if this plugin can cut the file.
/// \param fileName The file to check.
/// \return CUTTER_TRUE if plugin can cut the file selected and CUTTER_FALSE if the plugin can't cut it.
////////////////////////////////////////////////////
Cutter_UInt8 TestPlugin_CanCut(char *fileName);

////////////////////////////////////////////////////
/// \fn Cutter_UInt8 TestPlugin_CanPaste(char *fileName)
/// \brief Verifies if this plugin can paste the file.
/// \param fileName The file to check.
/// \return CUTTER_TRUE if plugin can paste the file selected and CUTTER_FALSE if the plugin can't paste it.
////////////////////////////////////////////////////
Cutter_UInt8 TestPlugin_CanPaste(char *fileName);

////////////////////////////////////////////////////
/// \fn void *TestPlugin_ShowAdditionalCutOptions(char *interface)
/// \brief Shows the additional cut options if has.
/// \param interface The interface to show the options.
/// \return Anything, it's interface-dependent.
////////////////////////////////////////////////////
void *TestPlugin_ShowAdditionalCutOptions(char *interface);

////////////////////////////////////////////////////
/// \fn void *TestPlugin_ShowAdditionalPasteOptions(char *interface)
/// \brief Shows the additional paste options if has.
/// \param interface The interface to show the options.
/// \return Anything, it's interface-dependent.
////////////////////////////////////////////////////
void *TestPlugin_ShowAdditionalPasteOptions(char *interface);

////////////////////////////////////////////////////
/// \fn Cutter_UInt8 TestPlugin_HasInterfaceSupport(char *interface)
/// \brief Verifies if has a explicit interface support.
/// \param interface The interface to check for availability.
/// \return CUTTER_TRUE if the interface is supported by the plugin, CUTTER_FALSE if the interface is not supported by the plugin.
////////////////////////////////////////////////////
Cutter_UInt8 TestPlugin_HasInterfaceSupport(char *interface);
