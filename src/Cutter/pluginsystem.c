//====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "pluginsystem.h"

//===========================================================//
//Global variables (Only in this file)                       //
//===========================================================//
static struct PluginSystem pluginsystem;
static GList *plugins = NULL; //An SplitterPlugin * array
static GList *modules = NULL; //An GModule * array

//===========================================================//
//Functions                                                  //
//===========================================================//

//Base implementation
struct PluginSystem *GetPluginSystem()
{
    pluginsystem.LoadPlugins = PluginSystem_LoadPlugins;
    pluginsystem.GetPluginNames =  PluginSystem_GetPluginNames;
    pluginsystem.ClosePlugins = PluginSystem_ClosePlugins;
    pluginsystem.GetPlugins = PluginSystem_GetPlugins;
    pluginsystem.UpdateProgressBar = NULL;

    return &pluginsystem;
}

GList *PluginSystem_GetPlugins()
{
    return plugins;
}

void PluginSystem_LoadPlugins(char *path, Cutter_UInt8 useProgressBar)
{
    GDir *directory; //This pointer must be freed.
    GList *pluginfiles = NULL; //This pointer must be freed.
    char *filePath; //This pointer must be freed.
    const char *fileName; //This pointer must not be freed.
    Cutter_UInt32 i;

    //Update the progress bar
    if((useProgressBar == CUTTER_TRUE) && (pluginsystem.UpdateProgressBar != NULL))
        pluginsystem.UpdateProgressBar(0, "Checking for plugins...", CUTTER_TRUE);

    directory = g_dir_open(path, 0, NULL);

#if defined(CUTTER_DEBUG)
    printf("(DEBUG) Checking for plugins...\n");
#endif

    if(directory != NULL)
    {
        //Check for plugins
        for(i = 0; (fileName = g_dir_read_name(directory)) != NULL; i++)
        {
            filePath = g_build_filename(path, fileName, NULL);

            if(PluginSystem_IsPlugin(filePath) == CUTTER_TRUE)
            {
                pluginfiles = g_list_append(pluginfiles, filePath);
            }
            else
            {
                g_free(filePath);
            }
        }

#if defined(CUTTER_DEBUG)
    printf("(DEBUG) Loading plugins...\n");
#endif

        //Update the progress bar
        if((useProgressBar == CUTTER_TRUE) && (pluginsystem.UpdateProgressBar != NULL))
                pluginsystem.UpdateProgressBar(10, "Checking for plugins...", CUTTER_FALSE);

        //Load the plugins
        for(i = 0; i < g_list_length(pluginfiles); i++)
        {
#if defined(CUTTER_DEBUG)
            printf("(DEBUG) Loading %s... ", (char *)g_list_nth_data(pluginfiles, i));
#endif
            //Open the module file
            GModule *modulefile = g_module_open((char *)g_list_nth_data(pluginfiles, i), G_MODULE_BIND_LAZY);

            if(modulefile != NULL)
            {
#if defined(CUTTER_DEBUG)
                printf("Loaded.\n");
#endif
                struct SplitterPlugin *(*GetPlugin)();

#if defined(CUTTER_DEBUG)
                printf("(DEBUG) Getting plugin structure inside...\n");
#endif

                //Load GetPlugin() function
                if (g_module_symbol(modulefile,"GetPlugin",(gpointer *)&GetPlugin) == FALSE)
                {
#if defined(CUTTER_DEBUG)
                    printf("(DEBUG) Failed to get structure.\n");
#endif
                    g_module_close(modulefile);
                    continue;
                }
                //Load plugin
                struct SplitterPlugin *plugin = GetPlugin();

#if defined(CUTTER_DEBUG)
                printf("(DEBUG) Loading plugin...\n");
#endif

                plugin->LoadPlugin();

#if defined(CUTTER_DEBUG)
                printf("(DEBUG) Plugin \"%s\" loaded sucessfuly.\n", plugin->Name);
#endif

                //Put plugin loaded and GModule in lists
                modules = g_list_append(modules, modulefile);
                plugins = g_list_append(plugins, GetPlugin());
            }

            //Update the progress bar
            if((useProgressBar == CUTTER_TRUE) && (pluginsystem.UpdateProgressBar != NULL) && (i = 0))
            {
                pluginsystem.UpdateProgressBar(10 + ((90 / g_list_length(pluginfiles)) * i), "Loading plugins...", CUTTER_TRUE);
            }
            else if((useProgressBar == CUTTER_TRUE) && (pluginsystem.UpdateProgressBar != NULL))
            {
                pluginsystem.UpdateProgressBar(10 + ((90 / g_list_length(pluginfiles)) * i), "Loading plugins...", CUTTER_FALSE);
            }
        }

        g_list_free(pluginfiles);
        g_dir_close(directory);
    }

    //Finish the update of the progress bar
    if((useProgressBar == CUTTER_TRUE) && (pluginsystem.UpdateProgressBar != NULL))
        pluginsystem.UpdateProgressBar(100, "Done.", CUTTER_TRUE);
}

void PluginSystem_ClosePlugins()
{
    Cutter_UInt32 i;
#if defined(CUTTER_DEBUG)
    GList *pluginnames = PluginSystem_GetPluginNames(); //This pointer must be freed.
#endif

    //Close the modules
    for(i = 0; i < g_list_length(modules); i++)
    {
#if defined(CUTTER_DEBUG)
        if(g_module_close((GModule *)g_list_nth_data(modules, i)) == TRUE)
        {
            printf("(DEBUG) Plugin %s closed correctly\n", (char *)g_list_nth_data(pluginnames,i));
        }
        else
        {
            printf("(DEBUG) Plugin %s closed unsucessfully\n", (char *)g_list_nth_data(pluginnames,i));
        }
#else
        g_module_close((GModule *)g_list_nth_data(modules, i));
#endif
    }

#if defined(CUTTER_DEBUG)
    g_list_free(pluginnames);
#endif

    //Free the plugins and the modules
    g_list_free(plugins);
    g_list_free(modules);
}

GList *PluginSystem_GetPluginNames()
{
    GList *names = NULL; //This pointer must be freed.
    GString *name; //This pointer must be freed.
    Cutter_UInt32 i;

    for(i = 0; i < g_list_length(plugins); i++)
    {
        name = g_string_new(NULL);

        g_string_printf(name, "%s v%i.%i (%s)", ((struct SplitterPlugin *)g_list_nth_data(plugins, i))->Name, ((struct SplitterPlugin *)g_list_nth_data(plugins, i))->PluginVersion.Major, ((struct SplitterPlugin *)g_list_nth_data(plugins, i))->PluginVersion.Minor, ((struct SplitterPlugin *)g_list_nth_data(plugins, i))->Provider);

        names = g_list_append(names, g_string_free(name, FALSE));
    }

    return names;
}

Cutter_UInt8 PluginSystem_IsPlugin(char *fileName)
{
    char *extension = strstr(fileName,".pln"); //This pointer must not be freed.
    GModule *module; //This pointer must be freed.

#if defined(CUTTER_DEBUG)
    printf("(DEBUG) IsPlugin(%s) [extension = %s] = ", fileName, extension);
#endif
    if((extension != NULL) && (strcmp(extension,".pln") == 0) && ((module = g_module_open(fileName, G_MODULE_BIND_LAZY)) != NULL))
    {
        g_module_close(module);

#if defined(CUTTER_DEBUG)
        printf("CUTTER_TRUE\n");
#endif
        return CUTTER_TRUE;
    }
#if defined(CUTTER_DEBUG)
        printf("CUTTER_FALSE\n");
#endif
    return CUTTER_FALSE;
}
