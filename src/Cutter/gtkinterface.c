/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//
#if defined(CUTTER_UI_GTK)
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "gtkinterface.h"

//===========================================================//
//Global variables                                           //
//===========================================================//

//General variables
//==============================
Cutter_Int32 currentstate;

//Load window
//==============================
GtkWindow *wnd_cutter_load;
GtkProgressBar *pbar_cutter_load_load;

//Cutter window
//==============================
//Main
GtkWindow *wnd_cutter_main;
GtkNotebook *ntbk_cutter_main_gui;
//Welcome
GtkLabel *lbl_cutter_main_welcome_title;
GtkLabel *lbl_cutter_main_welcome_title2;
GtkRadioButton *rbtn_cutter_main_welcome_cut;
GtkRadioButton *rbtn_cutter_main_welcome_paste;
GtkRadioButton *rbtn_cutter_main_welcome_exit;
//Cut
GtkLabel *lbl_cutter_main_cut_title;
GtkLabel *lbl_cutter_main_cut_file;
GtkLabel *lbl_cutter_main_cut_path;
GtkLabel *lbl_cutter_main_cut_size;
GtkLabel *lbl_cutter_main_cut_format;
GtkFileChooserButton *filechbtn_cutter_main_cut_file;
GtkFileChooserButton *filechbtn_cutter_main_cut_path;
GtkSpinButton *spnbtn_cutter_main_cut_size;
GtkComboBox *cmb_cutter_main_cut_size;
GtkComboBox *cmb_cutter_main_cut_format;
//Paste
GtkLabel *lbl_cutter_main_paste_title;
GtkLabel *lbl_cutter_main_paste_file;
GtkLabel *lbl_cutter_main_paste_path;
GtkLabel *lbl_cutter_main_paste_format;
GtkFileChooserButton *filechbtn_cutter_main_paste_file;
GtkFileChooserButton *filechbtn_cutter_main_paste_path;
GtkComboBox *cmb_cutter_main_paste_format;
//Cut options
GtkLabel *lbl_cutter_main_cutoptions_title;
GtkAlignment *algnmnt_cutter_main_cutoptions;
//Paste options
GtkLabel *lbl_cutter_main_pasteoptions_title;
GtkAlignment *algnmnt_cutter_main_pasteoptions;
//Processing
GtkLabel *lbl_cutter_main_processing_title;
GtkLabel *lbl_cutter_main_processing_progress;
GtkProgressBar *pbar_cutter_main_processing_progress;
//Message
GtkLabel *lbl_cutter_main_message_title;
GtkLabel *lbl_cutter_main_message_message;
GtkImage *img_cutter_main_message_icon;
//Actions
GtkButton *btn_cutter_main_action1;
GtkButton *btn_cutter_main_action2;
GtkButton *btn_cutter_main_action3;
GtkImage *img_cutter_main_action1;
GtkImage *img_cutter_main_action2;
GtkImage *img_cutter_main_action3;

//===========================================================//
//Functions                                                  //
//===========================================================//

void GTKInterface_Load(struct PluginSystem *system, int argc, char *argv[])
{
    GtkBuilder *builder;

    //Initialize GTK+
    gtk_set_locale();
    gtk_init(&argc, &argv);

    //Load GUI
    builder = gtk_builder_new();

    if(gtk_builder_add_from_file(builder,"gtk/cutter.ui", NULL) == 0)
    {
        printf("ERROR: Could not load GUI.\n");
        return;
    }

    gtk_builder_connect_signals(builder, NULL);

    //Get objects from the Load window
    //=================================================
    GTKBUILDER_GET_WIDGET(GtkWindow, wnd_cutter_load);
    GTKBUILDER_GET_WIDGET(GtkProgressBar, pbar_cutter_load_load);

    //Get objects from the Cutter window
    //=================================================
    //Main
    GTKBUILDER_GET_WIDGET(GtkWindow, wnd_cutter_main);
    GTKBUILDER_GET_WIDGET(GtkNotebook, ntbk_cutter_main_gui);
    //Welcome
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_welcome_title);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_welcome_title2);
    GTKBUILDER_GET_WIDGET(GtkRadioButton, rbtn_cutter_main_welcome_cut);
    GTKBUILDER_GET_WIDGET(GtkRadioButton, rbtn_cutter_main_welcome_paste);
    GTKBUILDER_GET_WIDGET(GtkRadioButton, rbtn_cutter_main_welcome_exit);
    //Cut
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cut_title);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cut_file);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cut_path);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cut_size);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cut_format);
    GTKBUILDER_GET_WIDGET(GtkFileChooserButton, filechbtn_cutter_main_cut_file);
    GTKBUILDER_GET_WIDGET(GtkFileChooserButton, filechbtn_cutter_main_cut_path);
    GTKBUILDER_GET_WIDGET(GtkSpinButton, spnbtn_cutter_main_cut_size);
    GTKBUILDER_GET_WIDGET(GtkComboBox, cmb_cutter_main_cut_size);
    GTKBUILDER_GET_WIDGET(GtkComboBox, cmb_cutter_main_cut_format);
    //Paste
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_paste_title);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_paste_file);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_paste_path);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_paste_format);
    GTKBUILDER_GET_WIDGET(GtkFileChooserButton, filechbtn_cutter_main_paste_file);
    GTKBUILDER_GET_WIDGET(GtkFileChooserButton, filechbtn_cutter_main_paste_path);
    GTKBUILDER_GET_WIDGET(GtkComboBox, cmb_cutter_main_paste_format);
    //Cut options
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_cutoptions_title);
    GTKBUILDER_GET_WIDGET(GtkAlignment, algnmnt_cutter_main_cutoptions);
    //Paste options
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_pasteoptions_title);
    GTKBUILDER_GET_WIDGET(GtkAlignment, algnmnt_cutter_main_pasteoptions);
    //Processing
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_processing_title);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_processing_progress);
    GTKBUILDER_GET_WIDGET(GtkProgressBar, pbar_cutter_main_processing_progress);
    //Message
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_message_title);
    GTKBUILDER_GET_WIDGET(GtkLabel, lbl_cutter_main_message_message);
    GTKBUILDER_GET_WIDGET(GtkImage, img_cutter_main_message_icon);
    //Actions
    GTKBUILDER_GET_WIDGET(GtkButton, btn_cutter_main_action1);
    GTKBUILDER_GET_WIDGET(GtkButton, btn_cutter_main_action2);
    GTKBUILDER_GET_WIDGET(GtkButton, btn_cutter_main_action3);
    GTKBUILDER_GET_WIDGET(GtkImage, img_cutter_main_action1);
    GTKBUILDER_GET_WIDGET(GtkImage, img_cutter_main_action2);
    GTKBUILDER_GET_WIDGET(GtkImage, img_cutter_main_action3);

    //Manual signals
    g_signal_connect(wnd_cutter_main, "delete-event", G_CALLBACK(gtk_main_quit), NULL);

    //Show load window
    gtk_widget_show_all(GTK_WIDGET(wnd_cutter_load));

    //Load plugins and select current progress bar update function to the load progress bar
    system->UpdateProgressBar = GTKInterface_UpdateLoadProgressBar;
    system->LoadPlugins("./plugins", CUTTER_TRUE);

    //Hide load window
    gtk_widget_hide_all(GTK_WIDGET(wnd_cutter_load));

    //Check if here is there is at least one plugin
    if(g_list_length(system->GetPlugins()) == 0)
	{
	    GTKInterface_ChangeState(NoPlugins, CUTTER_TRUE);
	}
	else
	{
        GTKInterface_ChangeState(Welcome, CUTTER_TRUE);
	}

    //Show main window
    gtk_widget_show_all(GTK_WIDGET(wnd_cutter_main));

    //Run GTK+ interface
    gtk_main();

    //Close plugins
    system->ClosePlugins();
}

void GTKInterface_UpdateLoadProgressBar(Cutter_UInt32 percent, char *text, Cutter_UInt8 updateText)
{
    //Update text
    if(updateText == CUTTER_TRUE)
        gtk_progress_bar_set_text(pbar_cutter_load_load, text);

    //Update progress
    gtk_progress_bar_update(pbar_cutter_load_load, ((gdouble)percent) / 100);

    //Refresh GTK+ application
    while(gtk_events_pending())
        gtk_main_iteration();
}

void GTKInterface_ChangeState(Cutter_Int32 state, Cutter_UInt8 clean)
{
    //Update state
    currentstate = state;

    switch(currentstate)
    {
        case Welcome:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui, 0);
            gtk_toggle_button_set_active((GtkToggleButton *)rbtn_cutter_main_welcome_cut, TRUE);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_ABOUT, GTK_STOCK_GO_FORWARD, NULL, "About", "Next");
            break;
        case Cut:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,1);
            GTKInterface_ModifyActionButtons(CUTTER_TRUE, CUTTER_TRUE, CUTTER_TRUE, GTK_STOCK_CANCEL, GTK_STOCK_GO_DOWN, GTK_STOCK_GO_FORWARD, "Cancel", "Previous", "Next");
            break;
        case Paste:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,2);
            GTKInterface_ModifyActionButtons(CUTTER_TRUE, CUTTER_TRUE, CUTTER_TRUE, GTK_STOCK_CANCEL, GTK_STOCK_GO_DOWN, GTK_STOCK_GO_FORWARD, "Cancel", "Previous", "Next");
            break;
        case CutOptions:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,3);
            GTKInterface_ModifyActionButtons(CUTTER_TRUE, CUTTER_TRUE, CUTTER_TRUE, GTK_STOCK_CANCEL, GTK_STOCK_GO_DOWN, GTK_STOCK_CUT, "Cancel", "Previous", "Cut");
            break;
        case PasteOptions:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,4);
            GTKInterface_ModifyActionButtons(CUTTER_TRUE, CUTTER_TRUE, CUTTER_TRUE, GTK_STOCK_CANCEL, GTK_STOCK_GO_DOWN, GTK_STOCK_PASTE, "Cancel", "Previous", "Paste");
            break;
        case Cutting: case Pasting:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,5);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE,CUTTER_FALSE,CUTTER_TRUE, NULL, NULL, GTK_STOCK_CANCEL, NULL, NULL, "Cancel");
            break;
        case CutSucessful:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,6);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_GOTO_FIRST, GTK_STOCK_QUIT, NULL, "Again", "Exit");
            break;
        case PasteSucessful:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,6);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_GOTO_FIRST, GTK_STOCK_QUIT, NULL, "Again", "Exit");
            break;
        case CutFailed:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,6);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_GOTO_FIRST, GTK_STOCK_QUIT, NULL, "Again", "Exit");
            break;
        case PasteFailed:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,6);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_GOTO_FIRST, GTK_STOCK_QUIT, NULL, "Again", "Exit");
            break;
        case NoPlugins:
            gtk_notebook_set_current_page(ntbk_cutter_main_gui,6);
            GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_FALSE, CUTTER_TRUE, NULL, NULL, GTK_STOCK_QUIT, NULL, NULL, "Exit");
            break;
    }
}

void GTKInterface_ModifyActionButtons(Cutter_Int32 action1Visible, Cutter_Int32 action2Visible, Cutter_Int32 action3Visible, char *action1Icon, char *action2Icon, char *action3Icon, char *action1Text, char *action2Text, char *action3Text)
{
    //Update visibles
    (action1Visible == CUTTER_TRUE) ? gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action1), TRUE) : gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action1), FALSE);
    (action2Visible == CUTTER_TRUE) ? gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action2), TRUE) : gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action2), FALSE);
    (action3Visible == CUTTER_TRUE) ? gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action3), TRUE) : gtk_widget_set_child_visible(GTK_WIDGET(btn_cutter_main_action3), FALSE);

    //Update stock images
    if (action1Icon != NULL)
        gtk_image_set_from_stock(img_cutter_main_action1, action1Icon, GTK_ICON_SIZE_BUTTON);
    if (action2Icon != NULL)
        gtk_image_set_from_stock(img_cutter_main_action2, action2Icon, GTK_ICON_SIZE_BUTTON);
    if (action3Icon != NULL)
        gtk_image_set_from_stock(img_cutter_main_action3, action3Icon, GTK_ICON_SIZE_BUTTON);

    //Update texts
    if (action1Text != NULL)
        gtk_button_set_label(btn_cutter_main_action1, action1Text);
    if (action2Text != NULL)
        gtk_button_set_label(btn_cutter_main_action2, action2Text);
    if (action3Text != NULL)
        gtk_button_set_label(btn_cutter_main_action3, action3Text);
}

void btn_cutter_main_action1_clicked(GtkButton *button, gpointer user_data)
{
    printf("Action1 clicked!\n");

    switch(currentstate)
    {
        case Cut:
            break;
    }
}

void btn_cutter_main_action2_clicked(GtkButton *button, gpointer user_data)
{
    printf("Action2 clicked!\n");

    switch(currentstate)
    {
        case Welcome:
            printf("About\n");
            break;
    }
}

void btn_cutter_main_action3_clicked(GtkButton *button, gpointer user_data)
{
    printf("Action3 clicked!\n");

    switch(currentstate)
    {
        case Welcome:
            if(gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_cut) == TRUE)
                GTKInterface_ChangeState(Cut, CUTTER_TRUE);
            if(gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_paste) == TRUE)
                GTKInterface_ChangeState(Paste, CUTTER_TRUE);
            if(gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_exit) == TRUE)
                gtk_main_quit();
            break;
    }
}

void rbtn_cutter_main_welcome_cut_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
    if((currentstate == Welcome) && (gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_cut) == TRUE))
        GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_ABOUT, GTK_STOCK_GO_FORWARD, NULL, "About", "Next");
}

void rbtn_cutter_main_welcome_paste_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
    if((currentstate == Welcome) && (gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_paste) == TRUE))
        GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_ABOUT, GTK_STOCK_GO_FORWARD, NULL, "About", "Next");
}

void rbtn_cutter_main_welcome_exit_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
    if((currentstate == Welcome) && (gtk_toggle_button_get_active((GtkToggleButton *)rbtn_cutter_main_welcome_exit) == TRUE))
        GTKInterface_ModifyActionButtons(CUTTER_FALSE, CUTTER_TRUE, CUTTER_TRUE, NULL, GTK_STOCK_ABOUT, GTK_STOCK_QUIT, NULL, "About", "Exit");
}

#endif
