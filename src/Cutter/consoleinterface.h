/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file consoleinterface.h
/// \brief Cutter console interface definitions.
/// \details This is the console intarface for use with Cutter.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//
#if !defined(CUTTERHEADER)

#include "cutter.h"

#endif

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void ConsoleInterface_Load(struct PluginSystem *system)
/// \brief Loads the console interface.
/// \param system The plugin system to use.
////////////////////////////////////////////////////
void ConsoleInterface_Load(struct PluginSystem *system, int argc, char *argv[]);

////////////////////////////////////////////////////
/// \fn static char *ConsoleInterface_AskFileName(Cutter_UInt8 checkFileName)
/// \brief Asks to put one file in the console.
/// \param checkFileName If is CUTTER_TRUE checks the existence of the file, if is CUTTER_FALSE don't checks it.
/// \return The file asked. This pointer must be freed.
////////////////////////////////////////////////////
char *ConsoleInterface_AskFileName(Cutter_UInt8 checkFileName);

////////////////////////////////////////////////////
/// \fn static char *ConsoleInterface_AskPath(Cutter_UInt8 checkPath)
/// \brief Asks to put one path in the console.
/// \param checkPath If is CUTTER_TRUE checks the existence of the path, if is CUTTER_FALSE don't checks it.
/// \return The path asked. This pointer must be freed.
////////////////////////////////////////////////////
char *ConsoleInterface_AskPath();

////////////////////////////////////////////////////
/// \fn static Cutter_UInt32 ConsoleInterface_AskOption(Cutter_UInt32 minValue, Cutter_UInt32 maxValue)
/// \brief Asks to put one numeric option in the console.
/// \param minValue The minimum value of the option.
/// \param maxValue The maximum value of the option.
/// \return The numeric option asked.
////////////////////////////////////////////////////
Cutter_UInt32 ConsoleInterface_AskOption(Cutter_UInt32 minValue, Cutter_UInt32 maxValue);

////////////////////////////////////////////////////
/// \fn static Cutter_UInt8 ConsoleInterface_ParseOption(GString *text, Cutter_UInt32 *option)
/// \brief Parses the text to the correct integer format of option.
/// \param text The text containing the option string.
/// \param option The option integer.
/// \return CUTTER_TRUE if the text is a valid option text, CUTTER_FALSE if text is not a valid option text.
////////////////////////////////////////////////////
Cutter_UInt8 ConsoleInterface_ParseOption(GString *text, Cutter_UInt32 *option);

////////////////////////////////////////////////////
/// \fn static Cutter_UInt64 ConsoleInterface_GetFileSize(char *fileName)
/// \brief Gets the file size in bytes.
/// \param fileName The file to use.
/// \return The file size in bytes or -1 on error.
////////////////////////////////////////////////////
Cutter_UInt64 ConsoleInterface_GetFileSize(char *fileName);

////////////////////////////////////////////////////
/// \fn static Cutter_UInt32 ConsoleInterface_AskAboutContinue()
/// \brief Asks about continue using Cutter.
/// \return 2 if exit Cutter, 1 if continue using Cutter.
////////////////////////////////////////////////////
Cutter_UInt32 ConsoleInterface_AskAboutContinue();

////////////////////////////////////////////////////
/// \fn static Cutter_UInt32 ConsoleInterface_AskFormat(struct PluginSystem *system)
/// \brief Asks for the format of the Cutter/Paster.
/// \param system The plugin system to use.
/// \return The index of the plugin to be used.
////////////////////////////////////////////////////
Cutter_UInt32 ConsoleInterface_AskFormat(struct PluginSystem *system);

////////////////////////////////////////////////////
/// \fn static char *ConsoleInterface_AskSourceFile(Cutter_UInt8 checkFileName)
/// \brief Asks for the source file to use.
/// \param checkFileName If is CUTTER_TRUE checks the existence of the file, if is CUTTER_FALSE don't checks it.
/// \return The source file to be used.
////////////////////////////////////////////////////
char *ConsoleInterface_AskSourceFile(Cutter_UInt8 checkFileName);

////////////////////////////////////////////////////
/// \fn static char *ConsoleInterface_AskDestinationPath()
/// \brief Asks for a destination path.
/// \return The destination path to be used.
////////////////////////////////////////////////////
char *ConsoleInterface_AskDestinationPath();

////////////////////////////////////////////////////
/// \fn static Cutter_UInt64 ConsoleInterface_AskSizeForCutFile(char *fileName)
/// \brief Asks for the size of the file to be cutted.
/// \param fileName The file to be cutted.
/// \return The size in bytes of the file.
////////////////////////////////////////////////////
Cutter_UInt64 ConsoleInterface_AskSizeForCutFile(char *fileName);
