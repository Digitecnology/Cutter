/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file pluginsystem.h
/// \brief Cutter plugin system definitions.
/// \details This is the plugin system for use with Cutter.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//
#if !defined(CUTTERHEADER)

#include "cutter.h"

#endif

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn struct PluginSystem *GetPluginSystem()
/// \brief Gets the plugin system.
/// \return The plugin system.
////////////////////////////////////////////////////
struct PluginSystem *GetPluginSystem();

////////////////////////////////////////////////////
/// \fn GList *PluginSystem_GetPlugins()
/// \brief Gets the plugins.
/// \return The list of SplitterPlugins.
////////////////////////////////////////////////////
GList *PluginSystem_GetPlugins();

////////////////////////////////////////////////////
/// \fn void PluginSystem_LoadPlugins(char *path, Cutter_UInt8 useProgressBar)
/// \brief Loads the plugins.
/// \param path The path for search for plugins.
/// \param useProgressBar If CUTTER_TRUE uses the progress bar, if CUTTER_FALSE don't uses it.
////////////////////////////////////////////////////
void PluginSystem_LoadPlugins(char *path, Cutter_UInt8 useProgressBar);

////////////////////////////////////////////////////
/// \fn void PluginSystem_ClosePlugins()
/// \brief Closes the plugins.
////////////////////////////////////////////////////
void PluginSystem_ClosePlugins();

////////////////////////////////////////////////////
/// \fn GList *PluginSystem_GetPluginNames()
/// \brief Gets the plugin names.
/// \return The list of plugin names. The list must be freed.
////////////////////////////////////////////////////
GList *PluginSystem_GetPluginNames();

////////////////////////////////////////////////////
/// \fn Cutter_UInt8 PluginSystem_IsPlugin(char *fileName)
/// \brief Checks if the filename specified is a Cutter plugin.
/// \param fileName The filename to check.
/// \return CUTTER_TRUE if the file is a Cutter plugin, CUTTER_FALSE if the file is not a Cutter plugin.
////////////////////////////////////////////////////
Cutter_UInt8 PluginSystem_IsPlugin(char *fileName);
