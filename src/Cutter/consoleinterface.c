/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//
#if defined(CUTTER_UI_CONSOLE)
//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "consoleinterface.h"

//===========================================================//
//Functions                                                  //
//===========================================================//

void ConsoleInterface_Load(struct PluginSystem *system, int argc, char *argv[])
{
	Cutter_UInt8 finish = CUTTER_FALSE;
	struct SplitterOperation result;

	char *source; //This pointer must be freed.
	Cutter_UInt64 size;
	char *destination; //This pointer must be freed.
	Cutter_UInt32 format;

	printf("=========================================\n");
	printf("Welcome to Cutter v" CUTTER_VERSION "\n");
	printf("=========================================\n\n");

	//Load the plugins
	printf("Loading plugins...\n");

	system->LoadPlugins("./plugins", CUTTER_FALSE);

	if(g_list_length(system->GetPlugins()) == 0)
	{
		printf("There are NO plugins installed. Please install plugins.\n\n");
		return;
	}

	printf("%u plugins loaded...\n", g_list_length(system->GetPlugins()));

	//Start the interface!!
	do
	{
		printf("\n=====================================\n");
		printf("What you want to do?:\n");
		printf("=====================================\n\n");
		printf("1)   Cut a file\n");
		printf("2)   Paste a file\n");
		printf("3)   Exit Cutter\n\n");

		switch(ConsoleInterface_AskOption(1, 3))
		{
			case 1: //Cut
				//==================================================================

				//Ask source file path
				source = ConsoleInterface_AskSourceFile(CUTTER_TRUE);

				//Ask the size of each cutted file
				size = ConsoleInterface_AskSizeForCutFile(source);

                //Ask format
                format = ConsoleInterface_AskFormat(system);

                //Ask destination path
				destination = ConsoleInterface_AskDestinationPath();

				printf("\n");

				//Verify if plugin can cut files
				if(((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->CanCut(source) == CUTTER_TRUE)
                {
                    //Search for additional plugin options...
                    if((((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->HasAdditionalCutOptions == CUTTER_TRUE) && (((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->HasInterfaceSupport("Console") == CUTTER_TRUE))
                        ((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->ShowAdditionalCutOptions("Console");

                    printf("\nCutting...\n");

                    //Cut the file...
                    result = ((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->CutFile(system, source, size, destination, CUTTER_FALSE);

                    //Verify the final status
                    switch(result.Status)
                    {
                        case Failed: //Operation failed
                            printf("File cutted unsucessfully.\n");
                            printf("%s\n",result.Message);
                            break;
                        case Finished: // Operation sucessful
                            printf("File cutted sucessfully.\n");
                            break;
                        case Warning: // Operation with a warning
                            printf("%s\n",result.Message);
                            break;
                    }
                }
                else
                {
                    printf("Can't cut the file.\n");
                }

                //Free the strings
                g_free(source);
                g_free(destination);

                //Ask about continue
                if(ConsoleInterface_AskAboutContinue() == 2)
                {
                    finish = CUTTER_TRUE;
                }
				break;
			case 2: //Paste
				//===================================================================

				//Ask source file path
				source = ConsoleInterface_AskSourceFile(CUTTER_TRUE);

				//Ask destination path
				destination = ConsoleInterface_AskDestinationPath();

				//Ask format
                format = ConsoleInterface_AskFormat(system);

                printf("\n");

                //Verify if plugin can paste files
                if(((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->CanPaste(source) == CUTTER_TRUE)
                {
                    //Search for additional plugin options...
                    if((((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->HasAdditionalPasteOptions == CUTTER_TRUE) && (((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->HasInterfaceSupport("Console") == CUTTER_TRUE))
                        ((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->ShowAdditionalPasteOptions("Console");

                    printf("\nPasting...\n");

                    //Paste file...
                    result = ((struct SplitterPlugin *)g_list_nth_data(system->GetPlugins(), format))->PasteFile(system, source, destination, CUTTER_FALSE);

                    //Verify the final status
                    switch(result.Status)
                    {
                        case Failed: //Operation failed
                            printf("File pasted unsucessfully.\n");
                            printf("%s\n",result.Message);
                            break;
                        case Finished: // Operation sucessful
                            printf("File pasted sucessfully.\n");
                            break;
                        case Warning: // Operation with a warning
                            printf("%s\n",result.Message);
                            break;
                    }
                }
                else
                {
                    printf("This plugin can't paste this file, sorry.\n");
                }

                //Free the strings
                g_free(source);
                g_free(destination);

                //Ask about continue
                if(ConsoleInterface_AskAboutContinue() == 2)
                {
                    finish = CUTTER_TRUE;
                }
				break;
			case 3: //Exit
				//===================================================================
				finish = CUTTER_TRUE;
				break;
		}
	}
	while(finish == CUTTER_FALSE);

	printf("\nBye bye!!\n\n");

	system->ClosePlugins();

	return;
}

//Function tested!!!
char *ConsoleInterface_AskFileName(Cutter_UInt8 checkFileName)
{
    GString *file = g_string_new(NULL); //This pointer must be freed.
    Cutter_UInt8 ask_finish = CUTTER_FALSE;
    char character;
    char *pathname; //This pointer must not be freed.

    do
    {
        printf("Please put a file location: ");

        while ((character = getchar()) != '\n')
            file = g_string_append_c(file, character);

        //Check for final separator, if there is one the file path is invalid
        if((strlen(file->str) > 0) && (file->str[strlen(file->str) - 1] == G_DIR_SEPARATOR))
        {
            printf("The file location is incorrect\n\n");
            g_string_erase(file, 0, -1);
            continue;
        }

        if((checkFileName == CUTTER_TRUE) && (g_file_test(file->str, G_FILE_TEST_EXISTS) == TRUE) && (g_file_test(file->str, G_FILE_TEST_IS_REGULAR) == TRUE)) //If the file exists and is a file
        {
            ask_finish = CUTTER_TRUE;
        }
        else if((checkFileName == CUTTER_FALSE) && (((strcmp((pathname = g_path_get_dirname(file->str)),".") != 0) && (g_path_is_absolute(file->str) == TRUE)) || ((strcmp((pathname = g_path_get_dirname(file->str)),".") == 0) && (g_path_is_absolute(file->str) == FALSE))) && (g_file_test(pathname, G_FILE_TEST_EXISTS) == TRUE) && (g_file_test(pathname, G_FILE_TEST_IS_DIR) == TRUE)) //If the file path is correct
        {
            if((g_file_test(file->str, G_FILE_TEST_EXISTS) == TRUE) && (g_file_test(file->str, G_FILE_TEST_IS_REGULAR) == TRUE)) //If the file exists and is a file
            {
                printf("The file specified exists. Overwrite? [1 = Yes, 0 = No].\n\n");

                if(ConsoleInterface_AskOption(0,1) == 1)
                {
                    if(g_remove(file->str) == 0)
                    {
                        ask_finish = CUTTER_TRUE;
                    }
                    else
                    {
                        printf("Could not overwrite file. Sorry...\n\n");
                        g_string_erase(file, 0, -1);
                    }
                }
            }
            else if(g_file_test(file->str, G_FILE_TEST_EXISTS) == FALSE) //If the file does not exist
            {
                ask_finish = CUTTER_TRUE;
            }
            else
            {
                printf("The file location is incorrect\n\n");
                g_string_erase(file, 0, -1);
            }
        }
        else
        {
            printf("The file location is incorrect\n\n");
            g_string_erase(file, 0, -1);
        }

    } while (ask_finish == CUTTER_FALSE);

    return g_string_free(file, FALSE);
}

//Function tested!!!
char *ConsoleInterface_AskPath()
{
    GString *file = g_string_new(NULL); //This pointer must be freed.
    Cutter_UInt8 ask_finish = CUTTER_FALSE;
    char character;

    do
    {
        printf("Please put a valid path: ");

        while ((character = getchar()) != '\n')
            file = g_string_append_c(file, character);

        if((g_file_test(file->str, G_FILE_TEST_EXISTS) == TRUE) && (g_file_test(file->str, G_FILE_TEST_IS_DIR) == TRUE))
        {
            ask_finish = CUTTER_TRUE;
        }
        else if(g_file_test(file->str, G_FILE_TEST_EXISTS) == FALSE)
        {
            printf("The directory specified does not exist. Make one? [1 = Yes, 0 = No].\n\n");

            if(ConsoleInterface_AskOption(0,1) == 1)
            {
                if(g_mkdir(file->str, 0777) == 0)
                {
                    ask_finish = CUTTER_TRUE;
                }
                else
                {
                    printf("Could not make the directory. Sorry...\n\n");
                }
            }
            else
            {
                g_string_erase(file, 0, -1);
            }
        }
        else
        {
                printf("The path is not valid.\n\n");
                g_string_erase(file, 0, -1);
        }

    } while (ask_finish == CUTTER_FALSE);

    //Remove final separator
    if(file->str[strlen(file->str) - 1] == G_DIR_SEPARATOR)
    {
        g_string_erase(file, strlen(file->str) - 1, 1);
    }

    return g_string_free(file, FALSE);
}

//Function tested!!!
Cutter_UInt32 ConsoleInterface_AskOption(Cutter_UInt32 minValue, Cutter_UInt32 maxValue)
{
	GString *text = g_string_new(NULL); //This pointer must be freed.
    Cutter_UInt8 ask_finish = CUTTER_FALSE;
    Cutter_UInt32 option;
	char character;

    do
    {
        printf("Put a valid option: ");

        while ((character = getchar()) != '\n')
                text = g_string_append_c(text, character);

        if((ConsoleInterface_ParseOption(text, &option) == CUTTER_TRUE) && (option >= minValue) && (option <= maxValue))
        {
            ask_finish = CUTTER_TRUE;
        }
        else
        {
            printf("Please, put a valid option.\n\n");
            g_string_erase(text, 0, -1);
        }

    } while (ask_finish == CUTTER_FALSE);

	g_string_free(text, TRUE);

    return option;
}

//Function tested!!
Cutter_UInt8 ConsoleInterface_ParseOption(GString *text, Cutter_UInt32 *option)
{
	Cutter_UInt32 i;
	Cutter_UInt32 count = 0;

	*option = 0;
	g_strreverse(text->str);

	//Text must have at maximum 6 characters
	if(strlen(text->str) > 6)
		return CUTTER_FALSE;

	//Test if all characters are digits
	for(i = 0; i < strlen(text->str); i++)
	{
		if(g_ascii_isdigit(text->str[i]) == TRUE)
            count++;
	}

	//If all are digits, convert the string to the option number
	if(count == strlen(text->str))
	{
		for(i = 0; i < strlen(text->str); i++)
			*option += (Cutter_UInt32)(text->str[i] - '0') * (Cutter_UInt32)pow(10.0,(double)i);

		return CUTTER_TRUE;
	}

	return CUTTER_FALSE;
}

//Function tested!!!
Cutter_UInt64 ConsoleInterface_GetFileSize(char *fileName)
{
    Cutter_UInt64 filesize;
    GMappedFile *file = g_mapped_file_new((const gchar *)fileName, FALSE, NULL); //This pointer must be freed.

	if(file != NULL)
	{
        filesize = g_mapped_file_get_length(file);

 		g_mapped_file_free(file);

        return filesize;
	}

	return -1;
}

Cutter_UInt32 ConsoleInterface_AskAboutContinue()
{
    printf("\n=====================================\n");
    printf("What you want to do?:\n");
    printf("=====================================\n");
    printf("1)   Again\n");
    printf("2)   Exit Cutter\n\n");

    return ConsoleInterface_AskOption(1, 2);
}

Cutter_UInt32 ConsoleInterface_AskFormat(struct PluginSystem *system)
{
    printf("\n=====================================\n");
    printf("Format:\n");
    printf("=====================================\n\n");

    GList *pluginnames = system->GetPluginNames(); //This pointer must be freed.
    Cutter_UInt32 i;

    for(i = 0; i < g_list_length(pluginnames); i++)
    {
        printf("%u) %s\n", i + 1, (char *)g_list_nth_data(pluginnames, i));
    }

    printf("\n");

    g_list_free(pluginnames);

    return ConsoleInterface_AskOption(1, i) - 1;
}

char *ConsoleInterface_AskSourceFile(Cutter_UInt8 checkFileName)
{
    printf("\n=====================================\n");
    printf("Source filename:\n");
    printf("=====================================\n\n");

    return ConsoleInterface_AskFileName(checkFileName);
}

char *ConsoleInterface_AskDestinationPath()
{
    printf("\n=====================================\n");
    printf("Destination path:\n");
    printf("=====================================\n\n");

    return ConsoleInterface_AskPath();
}

Cutter_UInt64 ConsoleInterface_AskSizeForCutFile(char *fileName)
{
    Cutter_UInt8 size_finish = CUTTER_FALSE;
    Cutter_UInt64 size;

    do
    {
        //Ask the size of each cutted file
        printf("\n=====================================\n");
        printf("Size:\n");
        printf("=====================================\n\n");

        size = (Cutter_UInt64)ConsoleInterface_AskOption(1,1024);

        //Ask the size unit
        printf("\n=====================================\n");
        printf("Size unit:\n");
        printf("=====================================\n\n");
        printf("1)   Bytes\n");
        printf("2)   KiloBytes\n");
        printf("3)   MegaBytes\n");
        printf("4)   GigaBytes\n\n");

        size *= (Cutter_UInt64)pow(1024,ConsoleInterface_AskOption(1, 4) - 1);

        //Check the size with the filesize
        if((size > 0) && (size < ConsoleInterface_GetFileSize(fileName)))
        {
            size_finish = CUTTER_TRUE;
        }
        else
        {
            printf("Put a valid size to cut the file.\n\n");
        }
    } while(size_finish == CUTTER_FALSE);

    return size;
}
#endif
