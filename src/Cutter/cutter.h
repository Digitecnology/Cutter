/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file cutter.h
/// \brief Cutter definitions.
/// \details This is the Cutter header for general purpose use.
////////////////////////////////////////////////////////////////////

#define CUTTERHEADER //Defined only for compiling purposes...

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gmodule.h>

//===========================================================//
//Macros                                                     //
//===========================================================//

////////////////////////////////////////////////////
/// \def CUTTER_TRUE
/// \brief Cutter TRUE boolean value.
////////////////////////////////////////////////////
#define CUTTER_TRUE 1

////////////////////////////////////////////////////
/// \def CUTTER_FALSE
/// \brief Cutter FALSE boolean value.
////////////////////////////////////////////////////
#define CUTTER_FALSE 0

////////////////////////////////////////////////////
/// \def CUTTER_VERSION
/// \brief The Cutter version.
////////////////////////////////////////////////////
#define CUTTER_VERSION "0.1"

//===========================================================//
//Types                                                      //
//===========================================================//

////////////////////////////////////////////////////
/// \typedef guint8 Cutter_UInt8
/// \brief The Cutter unsigned char.
////////////////////////////////////////////////////
typedef guint8 Cutter_UInt8;

////////////////////////////////////////////////////
/// \typedef gint8 Cutter_Int8
/// \brief The Cutter signed char.
////////////////////////////////////////////////////
typedef gint8 Cutter_Int8;

////////////////////////////////////////////////////
/// \typedef guint16 Cutter_UInt16
/// \brief The Cutter unsigned short.
////////////////////////////////////////////////////
typedef guint16 Cutter_UInt16;

////////////////////////////////////////////////////
/// \typedef gint16 Cutter_Int16
/// \brief The Cutter signed short.
////////////////////////////////////////////////////
typedef gint16 Cutter_Int16;

////////////////////////////////////////////////////
/// \typedef guint32 Cutter_UInt32
/// \brief The Cutter unsigned integer.
////////////////////////////////////////////////////
typedef guint32 Cutter_UInt32;

////////////////////////////////////////////////////
/// \typedef gint32 Cutter_Int32
/// \brief The Cutter signed integer.
////////////////////////////////////////////////////
typedef gint32 Cutter_Int32;

////////////////////////////////////////////////////
/// \typedef guint64 Cutter_UInt64
/// \brief The Cutter unsigned long integer.
////////////////////////////////////////////////////
typedef guint64 Cutter_UInt64;

////////////////////////////////////////////////////
/// \typedef gint64 Cutter_Int64
/// \brief The Cutter signed long integer.
////////////////////////////////////////////////////
typedef gint64 Cutter_Int64;

//===========================================================//
//Enumerations                                               //
//===========================================================//

////////////////////////////////////////////////////
/// \enum OperationStatus
/// \brief Enumeration to determine operation status of a splitter operation.
////////////////////////////////////////////////////
enum OperationStatus
{
	Finished = 0,
	Failed = 1,
	Warning = 2
};

//===========================================================//
//Structures                                                 //
//===========================================================//

////////////////////////////////////////////////////
/// \struct SplitterOperation
/// \brief The splitter operation structure.
////////////////////////////////////////////////////
struct SplitterOperation
{
	Cutter_Int32 Status;
	char *Message;
};

////////////////////////////////////////////////////
/// \struct Version
/// \brief A structure defining a version.
////////////////////////////////////////////////////
struct Version
{
	Cutter_Int32 Major;
	Cutter_Int32 Minor;
};

////////////////////////////////////////////////////
/// \struct PluginSystem
/// \brief A structure definining a plugin system. The main plugin system is called using the function struct PluginSystem *GetPluginSystem().
////////////////////////////////////////////////////
struct PluginSystem
{
	GList *(*GetPlugins)();
	void (*LoadPlugins)(char *path, Cutter_UInt8 useProgressBar);
	void (*ClosePlugins)();
	GList *(*GetPluginNames)();
	void (*UpdateProgressBar)(Cutter_UInt32 percent, char *text, Cutter_UInt8 updateText);
};

////////////////////////////////////////////////////
/// \struct SplitterPlugin
/// \brief A structure defining a Cutter plugin. Implement a Cutter plugin using the function struct SplitterPlugin *GetPlugin().
////////////////////////////////////////////////////
struct SplitterPlugin
{
    void (*LoadPlugin)();
    void (*ClosePlugin)();
	struct SplitterOperation (*CutFile)(struct PluginSystem *system, char *fileName, Cutter_UInt64 size, char *path, Cutter_UInt8 useProgressBar);
	struct SplitterOperation (*PasteFile)(struct PluginSystem *system, char *fileName, char *path, Cutter_UInt8 useProgressBar);
	Cutter_UInt8 (*CanCut)(char *fileName);
	Cutter_UInt8 (*CanPaste)(char *filename);
	void *(*ShowAdditionalCutOptions)(char *interface);
	void *(*ShowAdditionalPasteOptions)(char *interface);
	Cutter_UInt8 (*HasInterfaceSupport)(char *interface);
	const char *Name;
	const char *Provider;
	struct Version PluginVersion;
	Cutter_UInt8 HasAdditionalCutOptions;
	Cutter_UInt8 HasAdditionalPasteOptions;
};

////////////////////////////////////////////////////
/// \struct CutterInterface
/// \brief A structure definining a Cutter user interface.
////////////////////////////////////////////////////
struct CutterInterface
{
	void (*LoadInterface)(struct PluginSystem *system, int argc, char *argv[]);
};
