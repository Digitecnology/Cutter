/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

//===========================================================//
//Libraries                                                  //
//===========================================================//

#if !defined(CUTTER_UI_GTK) && !defined(CUTTER_UI_CONSOLE)

#error Please, select a least one interface!!!

#endif

#if !defined(CUTTERHEADER)

#include "cutter.h"

#endif

#include "pluginsystem.h"

#if defined(CUTTER_UI_CONSOLE)

#include "consoleinterface.h"

#endif

#if defined(CUTTER_UI_GTK)

#include "gtkinterface.h"

#endif

//===========================================================//
//Functions                                                  //
//===========================================================//

int main(int argc, char *argv[])
{
    struct PluginSystem *pluginsystem;
    struct CutterInterface cutterinterface;

	//Initialize plugin system
    pluginsystem = GetPluginSystem();

	if((argc == 2) && (strcmp(argv[1],"-gtk") == 0)) //Graphical Version
	{
#if defined(CUTTER_UI_GTK)
        //Assign GTK+ interface
		cutterinterface.LoadInterface = GTKInterface_Load;
#else
        //Assign GTK+ interface
		printf("GTK+ interface is not available...\n");
		return 0;
#endif
	}
	else if((argc == 2) && (strcmp(argv[1],"-console") == 0)) //Console Version
	{
#if defined(CUTTER_UI_CONSOLE)
		//Assign console interface
		cutterinterface.LoadInterface = ConsoleInterface_Load;
#else
        printf("Console interface is not available...\n");
        return 0;
#endif
	}
	else
	{
        printf("\nUsage: Cutter [-gtk] [-console] [-help]\n\n");
#if defined(CUTTER_UI_GTK)
        printf("-gtk: Executes Cutter via the GTK+ interface.\n");
#else
        printf("-gtk: This interface is not available in this build.\n");
#endif
#if defined(CUTTER_UI_CONSOLE)
        printf("-console: Executes Cutter via the console interface.\n");
#else
        printf("-console: This interface is not available in this build.\n");
#endif
        printf("-help: Shows Cutter help.\n\n");

        return 0;
    }

	//Load interface
	cutterinterface.LoadInterface(pluginsystem, argc, argv);

	return 0;
}
