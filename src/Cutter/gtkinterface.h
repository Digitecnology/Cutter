/*
    Cutter - A program to cut and paste files.
    Copyright (C) 2009  Steven Rodriguez

    This program is part of Cutter.

    Cutter is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the program has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//Digitecnology Cutter (2009 - Steven Rodriguez -)                     //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file gtkinterface.h
/// \brief Cutter GTK+ interface definitions.
/// \details This is the GTK+ interface for use with Cutter.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//
#if !defined(CUTTERHEADER)

#include "cutter.h"

#endif

#include <gtk/gtk.h>

//===========================================================//
//Macros                                                     //
//===========================================================//

#define GTKBUILDER_GET_WIDGET(x,y) y = (x *)gtk_builder_get_object(builder,#y)

//===========================================================//
//Enumerations                                               //
//===========================================================//

enum CutterGTKState
{
        Welcome = 0,
        Cut = 1,
        Paste = 2,
        CutOptions = 3,
        PasteOptions = 4,
        Cutting = 5,
        Pasting = 6,
        CutSucessful = 7,
        PasteSucessful = 8,
        CutFailed = 9,
        PasteFailed = 10,
        NoPlugins = 11
};

//===========================================================//
//Functions                                                  //
//===========================================================//

////////////////////////////////////////////////////
/// \fn void GTKInterface_Load(struct PluginSystem *system)
/// \brief Loads the GTK+ interface.
/// \param system The plugin system to use.
////////////////////////////////////////////////////
void GTKInterface_Load(struct PluginSystem *system, int argc, char *argv[]);
void GTKInterface_UpdateLoadProgressBar(Cutter_UInt32 percent, char *text, Cutter_UInt8 updateText);
void GTKInterface_ChangeState(Cutter_Int32 state, Cutter_UInt8 clean);
void GTKInterface_ModifyActionButtons(Cutter_Int32 action1Visible, Cutter_Int32 action2Visible, Cutter_Int32 action3Visible, char *action1Icon, char *action2Icon, char *action3Icon, char *action1Text, char *action2Text, char *action3Text);
